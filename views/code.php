<?php

session_start();

include('../connection.php');
//start delete podataka stranice
if (isset($_POST['delete_page'])) {
    //ovde dobijamo id iz button name atributa koji dobija vrednost iz value atributa od button taga
    $page_id = $_POST['delete_page'];

    try {
        $query = "DELETE FROM posts WHERE id=:page_id";

        $statement = $conn->prepare($query);

        $query_execute = $statement->execute([':page_id' => $page_id]);

        if ($query_execute) {
            $_SESSION['message'] = "Stranica je uspesno obrisana";
            header('Location: dashboard.php');
            exit(0);
        } else {
            $_SESSION['message'] = "Stranica nije obrisana";
            header('Location: dashboard.php');
            exit(0);
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}
//end delete podataka stranice

//start update podataka stranice
if (isset($_POST['update_page_btn'])) {

    $page_id = $_POST['page_id'];
    $title = $_POST['title'];
    $body = $_POST['body'];
    $is_published = $_POST['is_published'];

    try {
        $query = "UPDATE posts SET title=:title, body=:body, is_published=:is_published WHERE id=:page_id LIMIT 1";
        $statement = $conn->prepare($query);

        $data = [
            ':title' => $title,
            ':body' => $body,
            ':is_published' => $is_published,
            ':page_id' => $page_id,
        ];
        $query_execute = $statement->execute($data);
        if ($query_execute) {
            $_SESSION['message'] = "Stranica je uspesno izmenjena";
            header('Location: dashboard.php');
            exit(0);
        } else {
            $_SESSION['message'] = "Stranica nije izmenjena";
            header('Location: dashboard.php');
            exit(0);
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}
//end update podataka stranice

//ukoliko je setovano tj stuisnuto dugme za unos podataka stranice
if (isset($_POST['save_page_btn'])) {
    //pokupicemo vrednosti iz input polja
    $title = $_POST['title'];
    $body = $_POST['body'];
    $is_published = $_POST['is_published'];

    //to sve napisemo sql jezikom da te vrednosti ubaci u placeholdere u bazu i stavimo to u var $query
    $query = "INSERT INTO posts (title,	body, is_published ) VALUES (:title, :body, :is_published)";
    //ond pripremimo izjavu i stavimo tu vrednost u var 
    $query_run = $conn->prepare($query);

    //bindovanje vresnoti ili povezivanje vrednosti
    $data = [
        ':title' => $title,
        ':body' => $body,
        ':is_published' => $is_published,
    ];

    //izvrsi unos podataka u bazu
    $query_execute = $query_run->execute($data);

    //provera da li je izvrsen sql code i unos vrednosti u bazu
    if ($query_execute) {
        $_SESSION['message'] = "Kreirana stranica uspesno";
        header('Location: dashboard.php');
        exit(0);
    } else {
        $_SESSION['message'] = "Nije kreirana strana uspesno";
        header('Location: dashboard.php');
        exit(0);
    }
}
