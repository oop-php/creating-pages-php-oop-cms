<?php
session_start();
?>
<?php
include('./connection.php');
$query = "SELECT * FROM posts";
$statement =  $conn->prepare($query);
$statement->execute();
$statement->setFetchMode(PDO::FETCH_OBJ);
$result = $statement->fetchAll(); //PDO::FETCH_ASSOC
// var_dump($result);


?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <nav class="navbar navbar-expand-lg bg-body-tertiary">
                    <div class="container-fluid">
                        <!-- <a class="navbar-brand" href="#">Navbar</a> -->
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="/create-pages/">Home</a>
                                </li>
                                <?php
                                if ($result) {

                                    foreach ($result as $item) {
                                        if ($item->is_published) {
                                            // echo $item->is_published;
                                ?>
                                            <li class="nav-item">
                                                <a class="nav-link" href="page-template.php?id=<?= $item->id; ?>"><?php echo $item->title; ?></a>
                                            </li>
                                <?php
                                        }
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <div class="content-page">

        <div class="container">
            <?php
            if (isset($_GET['id'])) {
                $page_id = $_GET['id'];
                //bindovanje vrednost sa var $page_id
                $query = "SELECT * FROM posts WHERE id=:page_id LIMIT 1";
                $statement = $conn->prepare($query);
                $data = [':page_id' => $page_id];
                $statement->execute($data);

                $result = $statement->fetch(PDO::FETCH_OBJ);
                // var_dump($result);
            }
            // ispis podataka stranice iz baze
            if ($result) {
            ?>
                <h1 style="text-align: center;"><?php echo $result->title; ?></h1>
                <br><br>
                <p style="padding: 40px; font-size: 20px;">
                    <?php echo $result->body; ?>
                </p>
            <?php
            }
            ?>

        </div>

    </div>
    <?php
    include('./footer.php');
    ?>