<?php
include('../connection.php');
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>Sidebars · Bootstrap v5.0</title>
    <link rel="canonical" href="">
    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">

</head>

<body class="login-page">
    <div class="container">
        <h1>Izmeni stranicu</h1>
        <div class="card">
            <div class="card-header">
                <h3>
                    Izmeni podatke na stranici
                    <a href="dashboard.php" class="btn btn-danger float-end">BACK</a>
                </h3>
            </div>
            <div class="card-body">
                <?php
                if (isset($_GET['id'])) {
                    $page_id = $_GET['id'];
                    //bindovanje vrednost sa var $page_id
                    $query = "SELECT * FROM posts WHERE id=:page_id LIMIT 1";
                    $statement = $conn->prepare($query);
                    $data = [':page_id' => $page_id];
                    $statement->execute($data);

                    $result = $statement->fetch(PDO::FETCH_OBJ);
                }
                ?>


                <form method="POST" action="code.php">

                    <input type="hidden" name="page_id" value="<?= $result->id; ?>">

                    <div class="mb-3">
                        <label class="form-label">Naslov Stranice</label>
                        <input type="text" class="form-control" name="title" value="<?= $result->title; ?>">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Text Stranice</label>
                        <textarea class="form-control" name="body" id="" cols="30" rows="10"><?= $result->body; ?></textarea>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Aktiviraj stranicu</label>
                        <select class="form-control" name="is_published" id="is_published">
                            <option value="<?= $result->is_published; ?>"><?= $result->is_published; ?></option>
                            <option value="active">Prikazi Stranicu</option>
                            <option value="noactive">Ne prikazuj stranicu</option>
                        </select>
                    </div>
                    <button type="submit" name="update_page_btn" class="btn btn-primary">Snimi izmene</button>
                </form>
            </div>
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>

</html>