<?php

class CreatePostsTable
{

    public static function postsTable($conn)
    {
        try {
            $query = "CREATE TABLE IF NOT EXISTS posts(
                id INT AUTO_INCREMENT PRIMARY KEY,
                title VARCHAR(255) NOT NULL,
                body TEXT NOT NULL,
                is_published VARCHAR(50)
            )";
            $stm = $conn->prepare($query);
            $stm->execute();
        } catch (\Throwable $th) {
            die($th->getMessage());
        }
    }
}
